
## Create react app
npx create-react-app app --template=typescript
cd app
npm i sass


## Start app
cd app
export NODE_OPTIONS=--openssl-legacy-provider
npm run start


## Storybook
npx sb init

## Music Search.

mkdir -p music/pages/
mkdir -p music/components/
mkdir -p stories/music/

touch music/pages/MusicSearch.tsx
touch stories/music/MusicSearch.stories.tsx

touch music/components/SearchForm.tsx
touch stories/music/SearchForm.stories.tsx

touch music/components/SearchResults.tsx
touch stories/music/SearchResults.stories.tsx

touch music/components/AlbumCard.tsx
touch stories/music/AlbumCard.stories.tsx

