import { useState } from "react"
import { Button } from "../../stories/Button"
import { mockAlbums } from "../../stories/music/mockAlbums"
import { Album } from "../components/Album"
import { SearchForm } from "../components/SearchForm"
import { SearchResults } from "../components/SearchResults"

interface Props {
}

export const MusicSearch = ({ }: Props) => {
  const [currentQuery, setCurrentQuery] = useState('')
  const [results, setResults] = useState<Album[]>([])

  const search = () => {
    setResults(mockAlbums)
  }

  return (
    <div>
     
     <div className="container">
       <div className="row">
         <div className="col">
           <SearchForm query={currentQuery} onSearch={search}></SearchForm>
         </div>
       </div>
       <div className="row">
         <div className="col">
           <SearchResults albums={results}></SearchResults>
         </div>
       </div>
     </div>
    </div>
  )
}

