import { AlbumImage } from "./AlbumCard";

export interface Album {
  id: string;
  name: string;
  images: AlbumImage[];
}
