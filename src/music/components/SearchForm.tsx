import { useState } from "react"
import { Button } from "../../stories/Button"
import { Album } from "./Album"
import { AlbumCard } from "./AlbumCard"


interface Props {
  query: string,
  onSearch: (query: string) => void
}

export const SearchForm = ({ query, onSearch }: Props) => {
  const [currentQuery, setCurrentQuery] = useState(query)

  return (
    <div>
      <div className="input-group mb-3">
        <input type="text" className="form-control" placeholder="Search"
          value={currentQuery} onChange={e => setCurrentQuery(e.currentTarget.value)} />
        <Button label="Search" onClick={() => onSearch(currentQuery)} />
      </div>
    </div>
  )
}

