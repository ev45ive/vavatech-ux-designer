import { Button } from "../../stories/Button"
import { Album } from "./Album"


export type AlbumImage = {
  url: string
}

interface Props {
  /**
   * Album Data Enitty
   */
  album: Album
}

export const AlbumCard = ({ album }: Props) => {
  return (
    <div>
      <div className="card">
        <img src={album.images[0].url} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{album.name}</h5>
          {/* <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}
          <Button label="Details" primary={true} />
        </div>
      </div>
    </div>
  )
}

