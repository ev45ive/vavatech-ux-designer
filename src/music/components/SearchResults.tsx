import { Button } from "../../stories/Button"
import { Album } from "./Album"
import { AlbumCard } from "./AlbumCard"


interface Props {
  /**
   * Albums Data Enitty
   */
  albums: Album[]
}

export const SearchResults = ({ albums }: Props) => {
  return (
    <div>
      <div className="row row-cols-1 row-cols-md-4 g-0">
        {albums.map(album => <div className="col">
          <AlbumCard album={album}/>
        </div>)}
      </div>
    </div>
  )
}

