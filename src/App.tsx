import React from 'react';
import logo from './logo.svg';
import './App.scss';

function App() {
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col">
            <h1 className="display-1">Hello</h1>
            <p>Welcome to design systems!</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
