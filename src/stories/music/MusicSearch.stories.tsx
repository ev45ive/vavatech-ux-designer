

import { ComponentStory, ComponentMeta } from '@storybook/react';

import '../../App.scss'
import { MusicSearch } from '../../music/pages/MusicSearch';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Music/Search/MusicSearch',
  component: MusicSearch,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
  decorators: [
    // (Story) => <div style={{ maxWidth: '300px', margin: '0 auto' }}>
    //   {Story()}
    // </div>
  ]
} as ComponentMeta<typeof MusicSearch>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof MusicSearch> = (args) => <MusicSearch {...args} />;



export const EmptyMusicSearch = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
EmptyMusicSearch.args = {
};

export const FormWithInitialQuery = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
FormWithInitialQuery.args = {
};