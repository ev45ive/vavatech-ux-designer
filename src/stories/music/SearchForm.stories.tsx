

import { ComponentStory, ComponentMeta } from '@storybook/react';

import '../../App.scss'
import { SearchForm } from '../../music/components/SearchForm';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Music/Search/SearchForm',
  component: SearchForm,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
  decorators: [
    // (Story) => <div style={{ maxWidth: '300px', margin: '0 auto' }}>
    //   {Story()}
    // </div>
  ]
} as ComponentMeta<typeof SearchForm>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof SearchForm> = (args) => <SearchForm {...args} />;



export const EmptySearchForm = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
EmptySearchForm.args = {
  query: ''
};

export const FormWithInitialQuery = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
FormWithInitialQuery.args = {
  query: 'Batman'
};