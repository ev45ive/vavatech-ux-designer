

import { ComponentStory, ComponentMeta } from '@storybook/react';
import { AlbumCard } from '../../music/components/AlbumCard';

import '../../App.scss'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Music/Search/AlbumCard',
  component: AlbumCard,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
  decorators: [
    (Story) => <div style={{ maxWidth: '300px', margin: '0 auto' }}>
      {Story()}
    </div>
  ]
} as ComponentMeta<typeof AlbumCard>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof AlbumCard> = (args) => <AlbumCard {...args} />;


const mockAlbum = {
  id: '123',
  name: 'Album',
  images: [{ url: 'https://www.placecage.com/c/300/300' }]
}


export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  album: mockAlbum
};

export const VeryLongName = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
VeryLongName.args = {
  album: { ...mockAlbum, name: 'Very very long album name ...' }
};