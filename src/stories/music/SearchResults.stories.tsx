

import { ComponentStory, ComponentMeta } from '@storybook/react';

import '../../App.scss'
import { SearchResults } from '../../music/components/SearchResults';
import { mockAlbums } from './mockAlbums';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Music/Search/SearchResults',
  component: SearchResults,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
  decorators: [
    // (Story) => <div style={{ maxWidth: '300px', margin: '0 auto' }}>
    //   {Story()}
    // </div>
  ]
} as ComponentMeta<typeof SearchResults>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof SearchResults> = (args) => <SearchResults {...args} />;


export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  albums: mockAlbums
};

export const EmptyState = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
EmptyState.args = {
  albums: []
};